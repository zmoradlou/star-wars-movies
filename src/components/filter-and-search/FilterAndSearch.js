import React from "react";

const FilterAndSearch = props => {
	return (
		<>
			<div className="col-sm-3">
				<div className="dropdown">
					<button
						className="btn btn-outline-secondary btn-block dropdown-toggle pointer"
						type="button"
						id="dropdownMenuButton"
						data-toggle="dropdown"
						aria-haspopup="true"
						aria-expanded="false">
						Sort by...
					</button>
					<div className="dropdown-menu" aria-labelledby="dropdownMenuButton">
						<div className="dropdown-item pointer" href="#" onClick={() => props.onSort("episode")}>
							Episode
						</div>
						<div className="dropdown-item pointer" href="#" onClick={() => props.onSort("year")}>
							Year
						</div>
					</div>
				</div>
			</div>
			<div className="col-sm-9">
				<input type="text" className="form-control" placeholder="Type to search" onKeyUp={e => props.onSearch(e.target.value)} />
			</div>
		</>
	);
};

export default FilterAndSearch;
