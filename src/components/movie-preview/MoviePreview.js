import React from "react";

const MoviePreview = props => {
	return (
		<>
			{props.movie ? (
				<>

					<div className="row my-3 preview">
						<h3>{props.movie.fields.title}</h3>
					</div>
					<div className="row my-3 preview">
						<h6>{props.movie.fields.opening_crawl}</h6>
					</div>
					<div className="row my-3 preview">
						<h5>Directed by: {props.movie.fields.producer}</h5>
					</div>
				</>
			) : (
				"No movie selected"
			)}
		</>
	);
};

export default MoviePreview;
