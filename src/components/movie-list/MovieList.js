import React, {useState} from "react";

const MovieList = props => {
	return (
		<>
			{props.movies.map((movie, index) => (
				<div
					className="row pointer itemlist p-3"
					style={props.selected === movie ? {backgroundColor: "cyan"} : null}
					key={index}
					onClick={() => props.onSelect(index)}>
					<div className="col-sm-3 d-flex align-items-center">Episode {movie.fields.episode_id}</div>
					<div className="col-sm-6 d-flex align-items-center">{movie.fields.title}</div>
					<div className="col-sm-3 d-flex align-items-center">{movie.fields.release_date}</div>
				</div>
			))}
		</>
	);
};

export default MovieList;
