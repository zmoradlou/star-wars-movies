import React, {useState, useEffect} from "react";
import "./App.css";
import MovieList from "./components/movie-list/MovieList";
import axios from "axios";
import MoviePreview from "./components/movie-preview/MoviePreview";
import FilterAndSearch from "./components/filter-and-search/FilterAndSearch";

function App() {
	const [movies, setMovies] = useState([]);
	const [backUpMovies, setBackUpMovies] = useState([]);
	const [selectedMovie, setSelectedMovie] = useState(null);

	useEffect(() => {
		getMovies()
			.then(res => {
				setMovies(res.data);
				setBackUpMovies(res.data);
			})
			.catch(err => {
				console.log(err);
			});
	}, []);

	const getMovies = () => {
		const _movies = axios.get("https://star-wars-api.herokuapp.com/films");
		return _movies;
	};

	const onSelectMovieHandler = index => {
		setSelectedMovie(movies[index]);
	};

	const onSearchHandler = term => {
    setSelectedMovie(null);
		let _movies = [...backUpMovies];
		if (term) {
			const filteredMovies = _movies.filter(item => item.fields.title.toLowerCase().indexOf(term.toLowerCase()) !== -1);
			setMovies(filteredMovies);
		} else {
			setMovies(backUpMovies);
		}
	};

	const onSortHandler = sortType => {
		let _movies = [...movies];
		if (sortType === "year") {
			_movies.sort((a, b) => new Date(a.fields.release_date) - new Date(b.fields.release_date));
		}
		if (sortType === "episode") {
			_movies.sort((a, b) => a.fields.episode_id - b.fields.episode_id);
		}
		setMovies(_movies);
	};

	return (
		<div className="container-fluid">
			<div className="row p-3 filter">
				<FilterAndSearch onSearch={onSearchHandler} onSort={onSortHandler} />
			</div>
			<div className="row body">
				<div className="col-sm-6 list">
					<MovieList movies={movies} onSelect={onSelectMovieHandler} selected={selectedMovie} />
				</div>
				<div className="col-sm-6 preview px-5 py-3">
					<MoviePreview movie={selectedMovie} />
				</div>
			</div>
		</div>
	);
}

export default App;
